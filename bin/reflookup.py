import sys
import difflib
with open("/home/local/EMBAD/amlago/work/repos/refs.txt") as f:
    lookup_table = [ (line.split(';')[0], line.split(';')[1]) for line in filter(None, f.read().split('\n'))]
best_row = None
best_match = None

fix_arg = sys.argv[1].replace('\n', '')
print(sys.argv[1], file=sys.stderr)
print(fix_arg, file=sys.stderr)

for tst in lookup_table:
    s = difflib.SequenceMatcher(None, fix_arg, tst[0])
    blocks = s.get_matching_blocks()
    if best_match == None or blocks[0].size > best_match.size:
        best_match = blocks[0]
        best_row = tst
if (best_match != None):
    print(best_row[1] + fix_arg[best_match.size + best_match.b:])
