set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set shiftround
set expandtab
set number

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"Plugin 'scrooloose/nerdtree'	

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'https://git.wincent.com/command-t.git'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}
Plugin 'ycm-core/YouCompleteMe'
Plugin 'chrisbra/vim-diff-enhanced'

Plugin 'scrooloose/nerdcommenter'



" Plugin 'tpope/vim-sleuth'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" You Completeme conf
let g:ycm_python_binary_path = 'python3'
nnoremap ,gt :YcmCompleter GoTo<CR>
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
" Enable folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

set wildmode=longest,list,full
set wildmenu


:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

set hlsearch
set mouse=a

" I haven't found how to hide this function (yet)
function! RestoreRegister()
  let @" = s:restore_reg
  return ''
endfunction

function! s:Repl()
    let s:restore_reg = @"
    return "p@=RestoreRegister()\<cr>"
endfunction

" NB: this supports "rp that replaces the selection by the contents of @r
vnoremap <silent> <expr> p <sid>Repl()

cmap w!! w !sudo tee > /dev/null %

set tags=tags;/

function! IncSwitch()
    let cmd="find_match " . @%
    let file=system(cmd)
    exe "e "file
endfun



nnoremap <silent> <C-w>z :ZoomWin<CR>

:nmap <leader>h :call IncSwitch()<CR>
:nmap <leader>t :YcmCompleter GetType<CR>
:nmap <leader>d :YcmCompleter GoToDeclaration<CR>
:nmap <leader>i :YcmCompleter GoToDefinition<CR>
:nmap <leader>q :YcmCompleter FixIt<CR>
:nmap <leader>c :YcmForceCompileAndDiagnostics<CR>

:map Or 5<C-e>
:map Ox 5<C-y>

:set history=1000
:set list
:set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»

:set cc=80,120

nnoremap x "_x
vnoremap x "_x

set undofile " Maintain undo history between sessions
set undodir=~/.vim/undodir


set noea
