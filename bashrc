#
# ~/.bashrc
#

export HISTFILESIZE=200000
export HISTSIZE=200000
# Variaveis de ambiente importantes
export EDITOR=vim

## Configurando historico:
# Append ao inves de substituir:
shopt -s histappend
# Arquivos maiores:
# Ignorar duplicatas:
HISTCONTROL=ignoredups
# Ignorar alguns comanos:
HISTIGNORE='ls:pwd:history'
# Formato:
HISTTIMEFORMAT=`echo -e "\e[34m [%F %T] \e[0m"`
#HISTTIMEFORMAT='%F %T '
# Atualizar historico imediatamente:
PROMPT_COMMAND='lastReturnCode=$?; history -a'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## Aliases:
# ls com cores:
alias ls='ls --color=auto'
alias ll='ls --color=auto -l'
alias afeh='feh --scale-down --auto-zoom'
alias grep='grep --color=auto'

lastReturnCode=$?

hostColor=$((( $(echo "ibase=16; $(hostname | md5sum | awk '{ print $1 }' | cut -c1-8 | tr /a-z/ /A-Z/ )" | bc) + 87 ) % 256))
virtualenv_info(){
    # Get Virtual Env
    if [[ -n "$VIRTUAL_ENV" ]]; then
        # Strip out the path and just leave the env name
        venv="${VIRTUAL_ENV##*/}"
    else
        # In case you don't have one activated
        venv=''
    fi
    [[ -n "$venv" ]] && echo "($venv)"
}

# disable the default virtualenv prompt change
export VIRTUAL_ENV_DISABLE_PROMPT=1


## Linhas do bash:
if [[ $EUID -ne 0 ]]; then
    export PS1="\[\e[31m\]┏\[\e[36m\]\$(virtualenv_info)\${EBS_PS1}\[\e[39m\](\t)\[\e[31m\]\u\[\e[39m\]@\[\e[38;5;${hostColor}m\]\h\[\e[39m\]:\[\e[33m\]\w\[\e[39m\](\$lastReturnCode)"
    export PS1="${PS1}\n\[\e[31m\]┖─>\[\e[39m\]"

else
    export PS1="\[\e[31m\]┏\[\e[36m\]$(virtualenv_info)\${EBS_PS1}\[\e[39m\](\t)\[\e[31m\e[43m\e[1m\]\u\[\e[0m\]@\[\e[38;5;${hostColor}m\]\h\[\e[39m\]:\w\[\e[39m\](\$?)"
    export PS1="$PS1\n\[\e[31m\]┖─>\[\e[0m\]"
fi
#source $HOME/.local/bash-git-prompt/gitprompt.sh

#PS2='\[\e[1A\e[31m\]┠\[\e[39m\e[1B\e[5D\e[31m\]┖─> \[\e[39m\]'

PS2='\[\e7\e[1A\e[31m\]┠\[\e[39m\e8\e[31m\]┖─>\[\e[39m\]'


## Super TAB-completion:
. /usr/share/bash-completion/bash_completion

## Funcao util para matar jobs na bradar:
matajobs () {
    JOB=$1
    kill -9 $(jobs -p ${JOB})
}

sgrep () {
	SEARCH="$1"
	shift
	grep --color=auto -Hrin "$SEARCH" . $@ --exclude-dir=.svn --exclude-dir=build --exclude=tags
}

## grep pdfs in directory
# Usage: spdfgrep pattern [PATH [lines_before_and_after]]
spdfgrep () {
    PATTERN="$1"
    if [ $# -gt 1 ]; then
        SPATH="$2"
    else
        SPATH="."
    fi
    if [ $# -gt 2 ]; then
        GREPC="$3"
    else
        GREPC="2"
    fi
    find "$SPATH" -name '*.pdf' -exec sh -c 'pdftotext "{}" - | grep -i --with-filename -C '$GREPC' --label="{}" --color=always '"$PATTERN" \;
}



export PATH="/usr/lib/ccache:$PATH"


export GCC_COLORS=1
export CMAKE_CXX_FLAGS="-fdiagnostics-color=always"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH"


export LESS_TERMCAP_mb=$'\e[1;34m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
#. /etc/profile.d/ebs.sh


#workaround for st terminal:
tput smkx

# meus executaveis:
export PATH=${PATH}:${HOME}/.local/bin
